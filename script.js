//création de l'objet littéral movie
const movie = {
    title: "Memento",
    year: "2000",
    director: "Christopher Nolan",
    actors: ["Guy Pearce", "Carrie-Anne Moss", "Joe Pantoliano"],
    isAFavorite: true,
    rate: 4.2,
    duration: 0,

    //création d'une méthode pour convertir la durée du film en minutes
    setDuration: function (hours, minutes) {
        this.duration = hours * 60 + minutes; //sauvegarde le résultat dans duration
    }
};

//utiliser la méthode de l'objet movie pour convertir la durée du film
movie.setDuration(2, 28); 
console.log(movie.duration); //148
